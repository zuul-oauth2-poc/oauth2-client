package org.hung.oauthclient.web;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CounterController {
	
	@Autowired
	private OAuth2AuthorizedClientService authorizedClientService;
	
	@Autowired
	private RestTemplate echoService;

	@Autowired
	private OAuth2RestTemplate echoService2;
	
	private long counter = 0;
	
	@GetMapping("/counter")
	public long getCounter() {
		return counter++;
	}
	
	@GetMapping("/caller")
	public String caller() {//OAuth2AuthenticationToken authentication) {
		/*
		OAuth2AuthorizedClient authorizedClient = 
			this.authorizedClientService.loadAuthorizedClient(
					authentication.getAuthorizedClientRegistrationId(),
					authentication.getName());

		OAuth2AccessToken accessToken = authorizedClient.getAccessToken();
		*/
		//OAuth2ClientContext context = null;
		
		//OAuth2RestTemplate template = new OAuth2RestTemplate();
				
		String echoMsg = echoService.getForObject("http://localhost:9090/greeting", String.class);
		
		return echoMsg;
	}
	
	@GetMapping("/caller2")
	public String caller2(OAuth2AuthenticationToken authentication) {
		
		OAuth2AuthorizedClient authorizedClient = 
			this.authorizedClientService.loadAuthorizedClient(
					authentication.getAuthorizedClientRegistrationId(),
					authentication.getName());

		//OAuth2AccessToken come from xxx.oauth2.core package
		OAuth2AccessToken accessToken = authorizedClient.getAccessToken();

		//DefaultOAuth2AccessToken come from oauth2.common package		
		DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken.getTokenValue());
		
		OAuth2ClientContext context = new DefaultOAuth2ClientContext(token);

		AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
		resource.setClientId("plain-client-1");
		resource.setClientSecret("passsword");
		resource.setScope(Arrays.asList("select","update"));
		resource.setUserAuthorizationUri("http://localhost:7070/oauth/authorize");
		resource.setAccessTokenUri("http://localhost:7070/auth/token");
		//resource.setPreEstablishedRedirectUri("http://localhost:8080/login/oauth2/code/my-oauth-client1");
		resource.setUseCurrentUri(false);
		
		OAuth2RestTemplate template = new OAuth2RestTemplate(resource,context);
				
		String echoMsg = template.getForObject("http://localhost:9090/greeting", String.class);
		
		return echoMsg;
	}
	
	@Autowired
	private OAuth2ClientContext oauth2Context;

	@Bean
    public OAuth2RestTemplate createRestTemplate(OAuth2ClientContext clientContext) {
    	
		AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
		resource.setClientId("plain-client-1");
		resource.setClientSecret("passsword");
		resource.setScope(Arrays.asList("select","update"));
		resource.setUserAuthorizationUri("http://localhost:7070/oauth/authorize");
		resource.setAccessTokenUri("http://localhost:7070/auth/token");
		//resource.setPreEstablishedRedirectUri("http://localhost:8080/login/oauth2/code/my-oauth-client1");
		resource.setUseCurrentUri(false);
		
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, clientContext);
		return restTemplate;
    }
	
}
